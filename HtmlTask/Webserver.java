package HtmlTask;

import com.sun.xml.internal.fastinfoset.util.CharArray;

import javax.xml.stream.events.Characters;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * User: coursar
 * Date: 11/20/16
 * Time: 12:33 AM
 */
public class Webserver {
    public Webserver() {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            while (true) {
                final Socket socket = serverSocket.accept(); // что будет если сюда заключить try + resources?
                new Thread(new Runnable() {
                    public void handleRequest(Socket socket) {
                        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                            String line = reader.readLine();
                            StringTokenizer tokenizer = new StringTokenizer(line);
                            String firstWord = tokenizer.nextToken();
                            String secondWord = tokenizer.nextToken();

                            if (firstWord.equals("GET")) {
                                String path = "src/WebServer" + secondWord;
                                String response = "";
                                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));

                                String target;
                                while ((target = in.readLine()) != null)
                                   response = response + target;

                                in.close();
                                handleResponse(socket, 200, response);

                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                    public void handleResponse(Socket socket, int status, String message) {
                        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
                            writer.write("HTTP/1.0 200 OK \r\n");
                            writer.write("Server: Java webserver\r\n");
                            writer.write("Content-Length: " + message.length() + "\r\n");
                            writer.write("\r\n");
                            writer.write(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void run() {
                        handleRequest(socket);
                    }
                }).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Webserver();
    }
}
